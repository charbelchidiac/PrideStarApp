package com.pridestar.pridestarttestapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdError;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.LoadAdError;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.initialization.InitializationStatus;
import com.google.android.gms.ads.initialization.OnInitializationCompleteListener;
import com.google.android.gms.ads.rewarded.RewardItem;
import com.google.android.gms.ads.rewarded.RewardedAd;
import com.google.android.gms.ads.rewarded.RewardedAdCallback;
import com.google.android.gms.ads.rewarded.RewardedAdLoadCallback;

public class MainActivity extends AppCompatActivity {

    private AdView mAdViewBanner;
    private InterstitialAd mInterstitialAd;
    private RewardedAd rewardedAd;

    TextView lblPointCount;
    Button btnBanner, btnInterstitialAd, btnRewardAd;
    RelativeLayout viewLoading;

    //rewards points
    int points = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        MobileAds.initialize(this, new OnInitializationCompleteListener() {
            @Override
            public void onInitializationComplete(InitializationStatus initializationStatus) {

            }
        });

        initView();
        initListeners();
    }

    /**
     * Initialize widgets
     */
    private void initView()
    {
        btnInterstitialAd = findViewById(R.id.btnInterstitialAd);
        lblPointCount     = findViewById(R.id.lblPointCount);
        mAdViewBanner     = findViewById(R.id.adViewBanner);
        btnRewardAd       = findViewById(R.id.btnRewardAd);
        viewLoading       = findViewById(R.id.viewLoading);
        btnBanner         = findViewById(R.id.btnBanner);
    }

    /**
     * Initialize the buttons listeners
     */
    private void initListeners()
    {
        btnRewardAd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                initRewardsAd();
            }
        });

        btnInterstitialAd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                initInterstitialAd();
            }
        });

        btnBanner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadBanner();
            }
        });
    }

    private void initInterstitialAd()
    {
        toggleLoadingView(true);
        mInterstitialAd = new InterstitialAd(this);
        mInterstitialAd.setAdUnitId(Constants.interstitialAdId);
        mInterstitialAd.loadAd(new AdRequest.Builder().build());

        mInterstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                toggleLoadingView(false);
                mInterstitialAd.show();
            }

            @Override
            public void onAdFailedToLoad(LoadAdError loadAdError) {
                super.onAdFailedToLoad(loadAdError);
                toggleLoadingView(false);
            }
        });
    }

    private void initRewardsAd()
    {
        toggleLoadingView(true);
        rewardedAd = new RewardedAd(this, Constants.rewardAdId);

        RewardedAdLoadCallback adLoadCallback = new RewardedAdLoadCallback() {
            @Override
            public void onRewardedAdLoaded() {
                toggleLoadingView(false);
                RewardedAdCallback adCallback = new RewardedAdCallback() {

                    @Override
                    public void onUserEarnedReward(@NonNull RewardItem reward) {
                        points += 1;
                        lblPointCount.setText(String.valueOf(points));
                    }
                };
                rewardedAd.show(MainActivity.this, adCallback);
            }

            @Override
            public void onRewardedAdFailedToLoad(LoadAdError loadAdError) {
                super.onRewardedAdFailedToLoad(loadAdError);
                toggleLoadingView(false);
            }
        };
        rewardedAd.loadAd(new AdRequest.Builder().build(), adLoadCallback);
    }

    private void loadBanner() {
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdViewBanner.loadAd(adRequest);
        mAdViewBanner.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {

            }

            @Override
            public void onAdFailedToLoad(LoadAdError adError) {
            }

            @Override
            public void onAdOpened() {
            }
        });
    }

    /**
     * toggle the loading screen on and off for a better user experience
     * @param show
     */
    private void toggleLoadingView(boolean show) {
        viewLoading.setVisibility(show ? View.VISIBLE : View.GONE);
    }
}
